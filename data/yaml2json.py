#!/usr/bin/env python3
"""
Read YAML from stdin, convert to JSON and output to STDOUT.
"""

import yaml
import json
from sys import stdin

data = yaml.safe_load(stdin.read())
print(json.dumps(data))
